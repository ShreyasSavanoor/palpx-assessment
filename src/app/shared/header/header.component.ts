import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  navigationsListDesktop = [{
    name: 'Home'
  }, {
    name: 'Innovation Stories'
  }, {
    name: 'Technology'
  }, {
    name: 'Connect with 3M'
  }];
  navigationsListMobile = [{
    name: 'All Products'
  }, {
    name: 'Innovation'
  }, {
    name: 'Explore'
  }];
  screenWidth;
  desktop = false;
  mobile = false;
  constructor() { }

  ngOnInit() {
    this.onResize();
  }

  onResize() {
    this.screenWidth = window.innerWidth;
    if (this.screenWidth >= 768) {
      this.desktop = true;
      this.mobile = false;
    } else {
      this.mobile = true;
      this.desktop = false;
    }
  }

}
