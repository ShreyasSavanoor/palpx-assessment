import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  @Input() storiesList;
  @Input() block = false;
  @Input() inline = false;


  constructor() { }

  ngOnInit() {
  }

}
